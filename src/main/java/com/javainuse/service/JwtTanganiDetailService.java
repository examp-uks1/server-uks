package com.javainuse.service;

import com.javainuse.dao.StatusPasienDao;
import com.javainuse.dao.TanganiDao;
import com.javainuse.dao.TindakanDao;
import com.javainuse.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtTanganiDetailService {

    @Autowired
    private TanganiDao tanganiDao;
    private long id;

    public JwtTanganiDetailService() {
    }

    public DAOTangani save(TanganiDTO product) {

        DAOTangani newProduct = new DAOTangani();
        newProduct.setPasien(product.getPasien());
        newProduct.setPenanganan(product.getPenanganan());
        newProduct.setPenyakit(product.getPenyakit());
        newProduct.setTindakan(product.getTindakan());


        return tanganiDao.save(newProduct);
    }


    public Optional<DAOTangani> findById(Long id) {
        return Optional.ofNullable(tanganiDao.findById(id));
    }


    public List<DAOTangani> findAll() {
        List<DAOTangani> products = new ArrayList<>();
        tanganiDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOTangani product = tanganiDao.findById(id);
        tanganiDao.delete(product);
    }


    public DAOTangani update(Long id) {
        DAOTangani product = tanganiDao.findById(id);
        return tanganiDao.save(product);
    }

}