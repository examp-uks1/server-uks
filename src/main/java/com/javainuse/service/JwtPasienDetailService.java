package com.javainuse.service;


import com.javainuse.dao.PasienDao;
import com.javainuse.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPasienDetailService {

    @Autowired
    private PasienDao pasienDao;
    private long id;

    public JwtPasienDetailService() {
    }

    public DAOPasien save(PasienDTO product) {

        DAOPasien newProduct = new DAOPasien();
        newProduct.setStatus(product.getStatus());
        newProduct.setGuru(product.getGuru());
        newProduct.setSiswa(product.getSiswa());
        newProduct.setKaryawan(product.getKaryawan());

        return pasienDao.save(newProduct);
    }


    public Optional<DAOPasien> findById(Long id) {
        return Optional.ofNullable(pasienDao.findById(id));
    }


    public List<DAOPasien> findAll() {
        List<DAOPasien> products = new ArrayList<>();
        pasienDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOPasien product = pasienDao.findById(id);
        pasienDao.delete(product);
    }


    public DAOPasien update(Long id) {
        DAOPasien product = pasienDao.findById(id);
        return pasienDao.save(product);
    }

}