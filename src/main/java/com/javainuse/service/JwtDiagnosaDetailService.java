package com.javainuse.service;

import com.javainuse.dao.DiagnosaDao;
import com.javainuse.dao.GuruDao;
import com.javainuse.model.DAODiagnosa;
import com.javainuse.model.DiagnosaDTO;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.DAOGuru;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtDiagnosaDetailService {

    @Autowired
    private DiagnosaDao diagnosaDao;
    private long id;

    public JwtDiagnosaDetailService() {
    }

    public DAODiagnosa save(DiagnosaDTO product) {

        DAODiagnosa newProduct = new DAODiagnosa();
        newProduct.setPenyakit(product.getPenyakit());


        return diagnosaDao.save(newProduct);
    }


    public Optional<DAODiagnosa> findById(Long id) {
        return Optional.ofNullable(diagnosaDao.findById(id));
    }


    public List<DAODiagnosa> findAll() {
        List<DAODiagnosa> products = new ArrayList<>();
        diagnosaDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAODiagnosa product = diagnosaDao.findById(id);
        diagnosaDao.delete(product);
    }


    public DAODiagnosa update(Long id) {
        DAODiagnosa product = diagnosaDao.findById(id);
        return diagnosaDao.save(product);
    }

}