package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.dao.KaryawanDao;
import com.javainuse.model.DAOKaryawan;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.KaryawanDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtKaryawanDetailService {

    @Autowired
    private KaryawanDao karyawanDao;
    private long id;

    public JwtKaryawanDetailService() {
    }

    public DAOKaryawan save(KaryawanDTO product) {

        DAOKaryawan newProduct = new DAOKaryawan();
        newProduct.setNama(product.getNama());
        newProduct.setTempatLahir(product.getTempatLahir());
        newProduct.setTanggalLahir(product.getTanggalLahir());
        newProduct.setAlamat(product.getAlamat());


        return karyawanDao.save(newProduct);
    }


    public Optional<DAOKaryawan> findById(Long id) {
        return Optional.ofNullable(karyawanDao.findById(id));
    }


    public List<DAOKaryawan> findAll() {
        List<DAOKaryawan> products = new ArrayList<>();
        karyawanDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOKaryawan product = karyawanDao.findById(id);
        karyawanDao.delete(product);
    }


    public DAOKaryawan update(Long id) {
        DAOKaryawan product = karyawanDao.findById(id);
        return karyawanDao.save(product);
    }

}