package com.javainuse.service;

import com.javainuse.dao.StatusPasienDao;
import com.javainuse.dao.TindakanDao;
import com.javainuse.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtStatusPasienDetailService {

    @Autowired
    private StatusPasienDao statusPasienDao;
    private long id;

    public JwtStatusPasienDetailService() {
    }

    public DAOStatusPasien save(StatusPasienDTO product) {

        DAOStatusPasien newProduct = new DAOStatusPasien();
        newProduct.setStatus(product.getStatus());


        return statusPasienDao.save(newProduct);
    }


    public Optional<DAOStatusPasien> findById(Long id) {
        return Optional.ofNullable(statusPasienDao.findById(id));
    }


    public List<DAOStatusPasien> findAll() {
        List<DAOStatusPasien> products = new ArrayList<>();
        statusPasienDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOStatusPasien product = statusPasienDao.findById(id);
        statusPasienDao.delete(product);
    }


    public DAOStatusPasien update(Long id) {
        DAOStatusPasien product = statusPasienDao.findById(id);
        return statusPasienDao.save(product);
    }

}