package com.javainuse.service;

import com.javainuse.dao.DiagnosaDao;
import com.javainuse.dao.ObatDao;
import com.javainuse.model.DAODiagnosa;
import com.javainuse.model.DAOObat;
import com.javainuse.model.DiagnosaDTO;
import com.javainuse.model.ObatDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtObatDetailService {

    @Autowired
    private ObatDao obatDao;
    private long id;

    public JwtObatDetailService() {
    }

    public DAOObat save(ObatDTO product) {

        DAOObat newProduct = new DAOObat();
        newProduct.setObat(product.getObat());
        newProduct.setStock(product.getStock());
        newProduct.setExpired(product.getExpired());


        return obatDao.save(newProduct);
    }


    public Optional<DAOObat> findById(Long id) {
        return Optional.ofNullable(obatDao.findById(id));
    }


    public List<DAOObat> findAll() {
        List<DAOObat> products = new ArrayList<>();
        obatDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOObat product = obatDao.findById(id);
        obatDao.delete(product);
    }


    public DAOObat update(Long id) {
        DAOObat product = obatDao.findById(id);
        return obatDao.save(product);
    }

}