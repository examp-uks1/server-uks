package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.dao.SiswaDao;
import com.javainuse.model.DAOGuru;
import com.javainuse.model.DAOSiswa;
import com.javainuse.model.SiswaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtSiswaDetailService {

    @Autowired
    private SiswaDao siswaDao;
    private long id;

    public JwtSiswaDetailService() {
    }

    public DAOSiswa save(SiswaDTO product) {

        DAOSiswa newProduct = new DAOSiswa();
        newProduct.setNama(product.getNama());
        newProduct.setTempatLahir(product.getTempatLahir());
        newProduct.setKelas(product.getKelas());
        newProduct.setTanggalLahir(product.getTanggalLahir());
        newProduct.setAlamat(product.getAlamat());


        return siswaDao.save(newProduct);
    }


    public Optional<DAOSiswa> findById(Long id) {
        return Optional.ofNullable(siswaDao.findById(id));
    }


    public List<DAOSiswa> findAll() {
        List<DAOSiswa> products = new ArrayList<>();
        siswaDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOSiswa product = siswaDao.findById(id);
        siswaDao.delete(product);
    }


    public DAOSiswa update(Long id) {
        DAOSiswa product = siswaDao.findById(id);
        return siswaDao.save(product);
    }

}