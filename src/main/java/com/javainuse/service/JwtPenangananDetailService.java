package com.javainuse.service;

import com.javainuse.dao.PenangananDao;
import com.javainuse.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtPenangananDetailService {

    @Autowired
    private PenangananDao penangananDao;
    private long id;

    public JwtPenangananDetailService() {
    }

    public DAOPenanganan save(PenangananDTO product) {

        DAOPenanganan newProduct = new DAOPenanganan();
        newProduct.setPertolongan(product.getPertolongan());


        return penangananDao.save(newProduct);
    }


    public Optional<DAOPenanganan> findById(Long id) {
        return Optional.ofNullable(penangananDao.findById(id));
    }


    public List<DAOPenanganan> findAll() {
        List<DAOPenanganan> products = new ArrayList<>();
        penangananDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOPenanganan product = penangananDao.findById(id);
        penangananDao.delete(product);
    }


    public DAOPenanganan update(Long id) {
        DAOPenanganan product = penangananDao.findById(id);
        return penangananDao.save(product);
    }

}