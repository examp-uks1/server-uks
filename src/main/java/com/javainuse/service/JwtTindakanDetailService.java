package com.javainuse.service;

import com.javainuse.dao.TindakanDao;
import com.javainuse.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtTindakanDetailService {

    @Autowired
    private TindakanDao tindakanDao;
    private long id;

    public JwtTindakanDetailService() {
    }

    public DAOTindakan save(TindakanDTO product) {

        DAOTindakan newProduct = new DAOTindakan();
        newProduct.setTindakan(product.getTindakan());


        return tindakanDao.save(newProduct);
    }


    public Optional<DAOTindakan> findById(Long id) {
        return Optional.ofNullable(tindakanDao.findById(id));
    }


    public List<DAOTindakan> findAll() {
        List<DAOTindakan> products = new ArrayList<>();
        tindakanDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOTindakan product = tindakanDao.findById(id);
        tindakanDao.delete(product);
    }


    public DAOTindakan update(Long id) {
        DAOTindakan product = tindakanDao.findById(id);
        return tindakanDao.save(product);
    }

}