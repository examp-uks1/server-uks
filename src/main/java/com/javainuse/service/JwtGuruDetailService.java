package com.javainuse.service;

import com.javainuse.dao.GuruDao;
import com.javainuse.model.GuruDTO;
import com.javainuse.model.DAOGuru;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtGuruDetailService {

    @Autowired
    private GuruDao guruDao;
    private long id;

    public JwtGuruDetailService() {
    }

    public DAOGuru save(GuruDTO product) {

        DAOGuru newProduct = new DAOGuru();
        newProduct.setNama(product.getNama());
        newProduct.setTempatLahir(product.getTempatLahir());
        newProduct.setTanggalLahir(product.getTanggalLahir());
        newProduct.setAlamat(product.getAlamat());


        return guruDao.save(newProduct);
    }


    public Optional<DAOGuru> findById(Long id) {
        return Optional.ofNullable(guruDao.findById(id));
    }


    public List<DAOGuru> findAll() {
        List<DAOGuru> products = new ArrayList<>();
        guruDao.findAll().forEach(products::add);
        return products;
    }


    public void delete(Long id) {
        DAOGuru product = guruDao.findById(id);
        guruDao.delete(product);
    }


    public DAOGuru update(Long id) {
        DAOGuru product = guruDao.findById(id);
        return guruDao.save(product);
    }

}