package com.javainuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class DAOPenanganan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    @Column
    private String pertolongan;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPertolongan() {
        return pertolongan;
    }

    public void setPertolongan(String pertolongan) {
        this.pertolongan = pertolongan;
    }


}