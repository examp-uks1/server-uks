package com.javainuse.model;

public class TanganiDTO {

    private long id;
    private String catatan;
    private DAOPenanganan penanganan;
    private DAOPasien pasien;
    private DAODiagnosa penyakit;
    private DAOTindakan tindakan;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public DAOPenanganan getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(DAOPenanganan penanganan) {
        this.penanganan = penanganan;
    }

    public DAOPasien getPasien() {
        return pasien;
    }

    public void setPasien(DAOPasien pasien) {
        this.pasien = pasien;
    }

    public DAODiagnosa getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(DAODiagnosa penyakit) {
        this.penyakit = penyakit;
    }

    public DAOTindakan getTindakan() {
        return tindakan;
    }

    public void setTindakan(DAOTindakan tindakan) {
        this.tindakan = tindakan;
    }
}
