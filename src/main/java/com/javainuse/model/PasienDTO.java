package com.javainuse.model;

import javax.persistence.ManyToOne;

public class PasienDTO {

    private long id;

    private String keluhan;

    private DAOGuru guru;

    private DAOSiswa siswa;

    private DAOKaryawan karyawan;

    private DAOStatusPasien pasien;

    private DAOStatusPasien status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public DAOGuru getGuru() {
        return guru;
    }

    public void setGuru(DAOGuru guru) {
        this.guru = guru;
    }

    public DAOSiswa getSiswa() {
        return siswa;
    }

    public void setSiswa(DAOSiswa siswa) {
        this.siswa = siswa;
    }

    public DAOKaryawan getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(DAOKaryawan karyawan) {
        this.karyawan = karyawan;
    }

    public DAOStatusPasien getPasien() {
        return pasien;
    }

    public void setPasien(DAOStatusPasien pasien) {
        this.pasien = pasien;
    }

    public DAOStatusPasien getStatus() {
        return status;
    }

    public void setStatus(DAOStatusPasien status) {
        this.status = status;
    }
}
