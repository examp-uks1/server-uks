package com.javainuse.model;

import javax.persistence.Column;

public class PenangananDTO {
    private long id;
    @Column
    private String pertolongan;
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPertolongan() {
        return pertolongan;
    }

    public void setPertolongan(String pertolongan) {
        this.pertolongan = pertolongan;
    }
}
