package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "tangani")
public class DAOTangani {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    @Column
    private String catatan;
    @ManyToOne
    private DAOPenanganan penanganan;
    @ManyToOne
    private DAOPasien pasien;
    @ManyToOne
    private DAODiagnosa penyakit;
    @ManyToOne
    private DAOTindakan tindakan;

    public DAOTangani() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public DAOPenanganan getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(DAOPenanganan penanganan) {
        this.penanganan = penanganan;
    }

    public DAOPasien getPasien() {
        return pasien;
    }

    public void setPasien(DAOPasien pasien) {
        this.pasien = pasien;
    }

    public DAODiagnosa getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(DAODiagnosa penyakit) {
        this.penyakit = penyakit;
    }

    public DAOTindakan getTindakan() {
        return tindakan;
    }

    public void setTindakan(DAOTindakan tindakan) {
        this.tindakan = tindakan;
    }
}
