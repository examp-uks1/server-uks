package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "pasien")
public class DAOPasien {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;
    @Column
    private String keluhan;

    @ManyToOne
    private DAOGuru guru;
    @ManyToOne
    private  DAOSiswa siswa;
    @ManyToOne
    private DAOKaryawan karyawan;
    @ManyToOne
    private DAOStatusPasien status;



    public DAOPasien(long id, String keluhan, DAOGuru guru, DAOSiswa siswa, DAOKaryawan karyawan, DAOStatusPasien status) {
        this.id = id;
        this.keluhan = keluhan;
        this.guru = guru;
        this.siswa = siswa;
        this.karyawan = karyawan;
        this.status = status;
    }

    public DAOPasien() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public DAOGuru getGuru() {
        return guru;
    }

    public void setGuru(DAOGuru guru) {
        this.guru = guru;
    }

    public DAOSiswa getSiswa() {
        return siswa;
    }

    public void setSiswa(DAOSiswa siswa) {
        this.siswa = siswa;
    }

    public DAOKaryawan getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(DAOKaryawan karyawan) {
        this.karyawan = karyawan;
    }

    public DAOStatusPasien getStatus() {
        return status;
    }

    public void setStatus(DAOStatusPasien status) {
        this.status = status;
    }


}
