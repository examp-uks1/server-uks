package com.javainuse.model;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.Set;

public class StatusPasienDTO {
    private long id;

    private String status;

    @OneToMany(mappedBy = "status")
    Set<DAOStatusPasien> menu;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<DAOStatusPasien> getMenu() {
        return menu;
    }

    public void setMenu(Set<DAOStatusPasien> menu) {
        this.menu = menu;
    }
}
