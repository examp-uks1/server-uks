package com.javainuse.model;

import javax.persistence.Column;

public class TindakanDTO {
    private long id;
    @Column
    private String tindakan;
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }
}
