package com.javainuse.model;

import javax.persistence.Column;

public class DiagnosaDTO {
    private long id;
    @Column
    private String penyakit;
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(String penyakit) {
        this.penyakit = penyakit;
    }
}
