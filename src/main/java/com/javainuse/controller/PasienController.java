package com.javainuse.controller;

import com.javainuse.model.DAODiagnosa;
import com.javainuse.model.DAOPasien;
import com.javainuse.model.DiagnosaDTO;
import com.javainuse.model.PasienDTO;
import com.javainuse.service.JwtDiagnosaDetailService;
import com.javainuse.service.JwtPasienDetailService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class PasienController {

    public static final Logger logger = LoggerFactory.getLogger(PasienController.class);

    @Autowired
    private JwtPasienDetailService productDetailService;


    // -------------------Create a Product-------------------------------------------

    @RequestMapping(value = "/pasien/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody PasienDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", product);

        productDetailService.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/pasien/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOPasien>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<DAOPasien> products = productDetailService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }


    // -------------------Retrieve Single Product By Id------------------------------------------

    @RequestMapping(value = "/pasien/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Product with id {}", id);

        Optional<DAOPasien> product = productDetailService.findById(id);

        if (product == null) {
            logger.error("Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // ------------------- Update a Product ------------------------------------------------
    @RequestMapping(value = "/pasien/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody PasienDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Updating Product with id {}", id);

        Optional<DAOPasien> currentProduct = productDetailService.findById(id);

        if (currentProduct == null) {
            logger.error("Unable to update. Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Product with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentProduct.orElseThrow().setStatus(product.getStatus());
        currentProduct.orElseThrow().setGuru(product.getGuru());
        currentProduct.orElseThrow().setSiswa(product.getSiswa());
        currentProduct.orElseThrow().setKaryawan(product.getKaryawan());

        productDetailService.update(currentProduct.get().getId());
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);

    }

    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/pasien/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Book with id {}", id);

        productDetailService.delete(id);
        return new ResponseEntity<DAOPasien>(HttpStatus.NO_CONTENT);
    }

}