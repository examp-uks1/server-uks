package com.javainuse.controller;

import com.javainuse.model.*;
import com.javainuse.service.JwtStatusPasienDetailService;
import com.javainuse.service.JwtTanganiDetailService;
import com.javainuse.service.JwtTindakanDetailService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class TanganiController {

    public static final Logger logger = LoggerFactory.getLogger(TanganiController.class);

    @Autowired
    private JwtTanganiDetailService productDetailService;


    // -------------------Create a Product-------------------------------------------

    @RequestMapping(value = "/tangani/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createProduct(@RequestBody TanganiDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Creating Product : {}", product);

        productDetailService.save(product);

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/tangani/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<DAOTangani>> listAllProducts() throws SQLException, ClassNotFoundException {

        List<DAOTangani> products = productDetailService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }


    // -------------------Retrieve Single Product By Id------------------------------------------

    @RequestMapping(value = "/tangani/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Product with id {}", id);

        Optional<DAOTangani> product = productDetailService.findById(id);

        if (product == null) {
            logger.error("Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Product with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // ------------------- Update a Product ------------------------------------------------
    @RequestMapping(value = "/tangani/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody TanganiDTO product) throws SQLException, ClassNotFoundException {
        logger.info("Updating Product with id {}", id);

        Optional<DAOTangani> currentProduct = productDetailService.findById(id);

        if (currentProduct == null) {
            logger.error("Unable to update. Product with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Product with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentProduct.orElseThrow().setPasien(product.getPasien());
        currentProduct.orElseThrow().setPenanganan(product.getPenanganan());
        currentProduct.orElseThrow().setPenyakit(product.getPenyakit());
        currentProduct.orElseThrow().setTindakan(product.getTindakan());

        productDetailService.update(currentProduct.get().getId());
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);

    }

    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/tangani/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Book with id {}", id);

        productDetailService.delete(id);
        return new ResponseEntity<DAOTangani>(HttpStatus.NO_CONTENT);
    }

}