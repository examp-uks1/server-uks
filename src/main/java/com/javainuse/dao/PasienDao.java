package com.javainuse.dao;

import com.javainuse.model.DAOPasien;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PasienDao extends CrudRepository<DAOPasien, Integer> {
    DAOPasien findById(long id);
}
