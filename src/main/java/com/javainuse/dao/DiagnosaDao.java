package com.javainuse.dao;

import com.javainuse.model.DAODiagnosa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface DiagnosaDao extends CrudRepository<DAODiagnosa, Integer> {
    DAODiagnosa findById(long id);
}
