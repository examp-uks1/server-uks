package com.javainuse.dao;

import com.javainuse.model.DAOKaryawan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface KaryawanDao extends CrudRepository<DAOKaryawan, Integer> {
    DAOKaryawan findById(long id);
}
