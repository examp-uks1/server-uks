package com.javainuse.dao;

import com.javainuse.model.DAOObat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface ObatDao extends CrudRepository<DAOObat, Integer> {
    DAOObat findById(long id);
}
